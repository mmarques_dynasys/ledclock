28/07/2014 (data da ultima alteracao do ficheiro)

 - Sensor ZigBee de tempearatura
	- Conecta-se a uma rede ZigBee e envia tempearatura periodicamente.

--------------------------------------------------------------------------------
- V00.00 - 28/07/14
  - Autor: MM
  - Compilador de C: - Microchip XC8 1.32A
                     - +speed
  - Assembler      : - 
  - Linker         : - 
  - COFF to COD    : - 
  - COFF to HEX    : - 
  - IDE            : - MPLAB X IDE V2.10
  - Pasta: - C:\Users\mmarques\Documents\Projectos\LedClock\FW\Clock_V00_00.X
  - Linker: - C:\Users\mmarques\Documents\Projectos\LedClock\FW\Clock_V00_00.X
  - HW : LedClock V00_04
  - uC : PIC18F5K50
  - Cristal : Interno com PLL activo, Fclk=48MHz
  - FW : (utilizacao da memoria)
    - Codigo       ????? bytes (de um total de ???Kbytes).
    - RAM interna:  ???? bytes.
    - RAM externa: ????? bytes.
  - Versao baseada na:
    - Versao original
  - Objectivo:
    - Configuracao do uC.
  - Funcionalidades:
    - Comuta um led a frequencia de 1Hz.
  - Notas:
    -
  - Erros:
    -
  - Sugestoes:
    -

-end-
