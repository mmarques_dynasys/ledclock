/*******************************************************************************
 * @file neoPixel.h
 * @brief
 * @details
 * @author MM
 * @version
 * @date 23 de Julho de 2014
 * @copyright Dynasys 
 ******************************************************************************/

#ifndef NEOPIXEL_H
#define	NEOPIXEL_H

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include "mytypes.h"

/*******************************************************************************
 * Public functions prototypes
 ******************************************************************************/

/**
 * @fn        NeoPixel_bit
 * @brief     Generates a bit for the RGB led.
 * @param[in] status Bit value to generate
 * @return    None
 */
void NeoPixel_bit(uint8_t status);

/**
 * @fn     NeoPixel_ini
 * @brief  Module initialization
 * @param  None
 * @return None
 */
void NeoPixel_ini(void);

#endif	/* NEOPIXEL_H */

/* --------------------------------- End Of File ---------------------------- */
