/*******************************************************************************
 * @file rtcc.h
 * @brief Functions to control the MCP7940N RTCC.
 * @author MM
 * @version 00.00
 * @date 01/10/14
 * @copyright MM
 ******************************************************************************/

#ifndef RTCC_H
#define	RTCC_H

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include "mytypes.h"

/*******************************************************************************
 * Typedefs
 ******************************************************************************/

typedef struct {
    uint8_t sec;
    uint8_t min;
    uint8_t hour;
    uint8_t dayW;
    uint8_t day;
    uint8_t month;
    uint8_t year;
} T_Rtcc_time;

/*******************************************************************************
 * Public functions prototypes
 ******************************************************************************/

/**
 * @fn        Rtcc_dec2BCD
 * @brief     Decimal to BCD convertion.
 * @param[in] dec Time struct in decimal.
 * @return    Time struct in BCD.
 */
T_Rtcc_time Rtcc_dec2BCD(T_Rtcc_time dec);

/**
 * @fn        Rtcc_BCD2Dec
 * @brief     BCD to decimal convertion.
 * @param[in] bcd Time struct in BCD.
 * @return    Time struct in decimal.
 */
T_Rtcc_time Rtcc_BCD2Dec(T_Rtcc_time bcd);

void Rtcc_writeTime(T_Rtcc_time time);

/**
 * @fn     Rtcc_readTime
 * @brief  Rtcc time and date registers read.
 * @param  None.
 * @return Time struct in BCD.
 */
T_Rtcc_time Rtcc_readTime(void);

/**
 * @fn     Rtcc_ini
 * @brief  Module initialization
 * @param  None
 * @return None
 */
void Rtcc_ini(void);


#endif	/* RTCC_H */

/* --------------------------------- End Of File ---------------------------- */
