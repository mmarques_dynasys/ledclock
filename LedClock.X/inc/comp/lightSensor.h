/*******************************************************************************
 * @file lightSensor.h
 * @brief
 * @details
 * @author MM
 * @version
 * @date 2 de Novembro de 2014
 * @copyright Dynasys 
 ******************************************************************************/

#ifndef LIGHTSENSOR_H
#define	LIGHTSENSOR_H

/*******************************************************************************
 * Includes
 ******************************************************************************/

/*******************************************************************************
 * Defines and typedefs
 ******************************************************************************/

/*******************************************************************************
 * Public functions prototypes
 ******************************************************************************/

uint16_t CalculateLux(uint8_t iGain, uint8_t tInt, uint16_t ch0, uint16_t ch1, uint8_t iType);

uint16_t LightSensor_readValue(uint8_t chnl);

/**
 * @fn     LightSensor_ini
 * @brief  Module initialization
 * @param  None
 * @return None
 */
void LightSensor_ini(void);

#endif	/* LIGHTSENSOR_H */

/* --------------------------------- End Of File ---------------------------- */
