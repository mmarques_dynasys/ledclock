/*******************************************************************************
 * @file timerTicks.h
 * @brief Funcoes para manipulacao de uma lista de timers.
 * @details Os timer contam ate existir overflow do MSB da vari�vel de contagem,
 * assinalando-se assim o fim da contagem. Atraves da definicao de
 * TIMERTICKS_16BITS comuta-se entre um timer que conta ate 21474836,48s(32bits)
 *  ou 327,68s(16bits), para uma contagem base de 10ms
 * @author MM
 * @version 03.00
 * @date 31/01/13
 * @copyright Dynasys
 ******************************************************************************/

#ifndef TIMERTICKS_H
#define TIMERTICKS_H

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include "mytypes.h"

/*******************************************************************************
 * Defines and typedefs
 ******************************************************************************/

#define TIMERTICKS_MS 10L                   //Tempo em ms entre 2 interrupts

typedef enum {                                       /* Lista de timers    */
    TIMERTICKS_MAINLED		,
    TIMERTICKS_PIXEL		,
    TIMERTICKS_BTNDEB           ,
    TIMERTICKS_COUNT
} T_TimerTicks_list;

#define TimerTicks_setMainLed(v)    TimerTicks_setTimer(TIMERTICKS_MAINLED,v)
#define TimerTicks_setPixel(v)      TimerTicks_setTimer(TIMERTICKS_PIXEL,v)
#define TimerTicks_setBtnDeb(v)     TimerTicks_setTimer(TIMERTICKS_BTNDEB,v)

#define TimerTicks_isMainLed()	    TimerTicks_isTimer(TIMERTICKS_MAINLED)
#define TimerTicks_isPixel()	    TimerTicks_isTimer(TIMERTICKS_PIXEL)
#define TimerTicks_isBtnDeb()	    TimerTicks_isTimer(TIMERTICKS_BTNDEB)

#define TimerTicks_dezsecToTics(v)    ((((uint32_t)(v))*10000L)/(TIMERTICKS_MS))
#define TimerTicks_secToTics(v)       ((((uint32_t)(v))*1000L)/(TIMERTICKS_MS))
#define TimerTicks_dsecToTics(v)      ((((uint32_t)(v))*100L)/(TIMERTICKS_MS))
#define TimerTicks_csecToTics(v)      ((((uint32_t)v)*10L)/(TIMERTICKS_MS))
#define TimerTicks_msecToTics(v)      (((uint32_t)(v))/(TIMERTICKS_MS))

/*******************************************************************************
 * Public functions prototypes
 ******************************************************************************/

/**
 * @fn     TimerTicks_interr
 * @brief  Interrupcao do timer, todos os timers da lista sao incrementados.
 * @param  None
 * @return None
 */
void TimerTicks_interr(void);

/**
 * @fn        TimerTicks_isTimer
 * @brief     Verifica se o timer atingiu a contagem (set do MSB).
 * @param[in] timer Indice na lista do timer a verificar.
 * @retval    TRUE Timer atingiu contagem
 * @retval    FALSE Timer n�o atingiu contagem
 */
Boolean TimerTicks_isTimer ( uint8_t timer );


/**
 * @fn        TimerTicks_setTimer
 * @brief     Inicializa timer indicado, com a temporizacao desejada.
 * @param[in] timer Indice na lista do timer a inicializar.
 * @param[in] delta Equivalente em ticks para a temporizacao desejada.
 * @return None
 */
void TimerTicks_setTimer ( uint8_t timer, uint32_t delta );

/**
 * @fn     TimerTicks_ini
 * @brief  Inicializacao do modulo
 * @param  None
 * @return None
 */
void TimerTicks_ini ( void );

#endif    /*TIMERTICKS_H*/

/* ------------------------------- End Of File ------------------------------ */
