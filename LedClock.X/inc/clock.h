/*******************************************************************************
 * @file clock.c
 * @brief Main control of the clock.
 * @author MM
 * @version 00.00
 * @date 01/10/14
 * @copyright MM
 ******************************************************************************/

#ifndef CLOCK_H
#define	CLOCK_H

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include "mytypes.h"

/*******************************************************************************
 * Defines and typedefs
 ******************************************************************************/

/*******************************************************************************
 * Public functions prototypes
 ******************************************************************************/

void Clock_interrExt2(void);

/**
 * @fn     Clock_drawTimeExecute
 * @brief  Rtcc read and time presentation.
 * @param  None
 * @retval ENDED Function terminated with success.
 * @retval ONGOING Executing function.
 */
T_Cycle Clock_drawTimeExecute(void);

void Clock_run(void);

/**
 * @fn     Clock_ini
 * @brief  Module initialization
 * @param  None
 * @return None
 */
void Clock_ini(void);

#endif	/* CLOCK_H */

/* --------------------------------- End Of File ---------------------------- */
