/*******************************************************************************
 *  @file mytypes.h
 *  @brief Definicao de tipos
 *  @details Definicao de tipos para o compilador Microchip C18
 *  @author MM
 *  @version 03.02
 *  @date 22/01/13
 *  @copyright Dynasys 2013
 ******************************************************************************/

#ifndef MYTYPES_H
#define MYTYPES_H

/*******************************************************************************
 * Defines and typedefs
 ******************************************************************************/

//typedef enum { TRUE=1, FALSE=0 } Boolean;           // Undefined size

//typedef enum { OK=1, NOK=0, ERROR=-1 } T_Status;
#define Boolean BOOL

typedef enum { ENDED=1, ONGOING=0, ERROR=-1 } T_Cycle;

typedef signed char          int8_t;            // 8-bit signed
typedef unsigned char        uint8_t;           // 8-bit unsigned

typedef signed short int     int16_t;           // 16-bit signed
typedef unsigned short int   uint16_t;          // 16-bit unsigned

typedef signed long          int32_t;           // 32-bit signed
typedef unsigned long int    uint32_t;          // 32-bit unsigned

typedef union {
    uint32_t int32_a[2];
    uint16_t int16_a[4];
    uint8_t int8_a[8];
    struct {
        uint32_t l;
        uint32_t h;
    } int64;
} uint64b_t;

typedef union {
    struct {
        unsigned b0 :1;
        unsigned b1 :1;
        unsigned b2 :1;
        unsigned b3 :1;
        unsigned b4 :1;
        unsigned b5 :1;
        unsigned b6 :1;
        unsigned b7 :1;
    } bits;
    uint8_t int8;
} uint8b_t;

typedef union {
    uint16_t int16;
    uint8_t int8_a[2];
    struct {
        uint8_t l; // Low byte  -> low address
        uint8_t h; // High byte -> high address
    } int8;
    struct {
        unsigned char b0 :1;
        unsigned char b1 :1;
        unsigned char b2 :1;
        unsigned char b3 :1;
        unsigned char b4 :1;
        unsigned char b5 :1;
        unsigned char b6 :1;
        unsigned char b7 :1;
        unsigned char b8 :1;
        unsigned char b9 :1;
        unsigned char b10 :1;
        unsigned char b11 :1;
        unsigned char b12 :1;
        unsigned char b13 :1;
        unsigned char b14 :1;
        unsigned char b15 :1;
    } bits;
} uint16b_t;

typedef union {
    uint32_t int32;
    uint16_t int16_a[2];
    uint8_t int8_a[4];
    struct {
        uint16_t l;
        uint16_t h;
    } int16;
    struct {
        uint8_t l;
        uint8_t h;
        uint8_t u;
        uint8_t m;
    } int8;
    struct {
        unsigned char b0 :1;
        unsigned char b1 :1;
        unsigned char b2 :1;
        unsigned char b3 :1;
        unsigned char b4 :1;
        unsigned char b5 :1;
        unsigned char b6 :1;
        unsigned char b7 :1;
        unsigned char b8 :1;
        unsigned char b9 :1;
        unsigned char b10 :1;
        unsigned char b11 :1;
        unsigned char b12 :1;
        unsigned char b13 :1;
        unsigned char b14 :1;
        unsigned char b15 :1;
        unsigned char b16 :1;
        unsigned char b17 :1;
        unsigned char b18 :1;
        unsigned char b19 :1;
        unsigned char b20 :1;
        unsigned char b21 :1;
        unsigned char b22 :1;
        unsigned char b23 :1;
        unsigned char b24 :1;
        unsigned char b25 :1;
        unsigned char b26 :1;
        unsigned char b27 :1;
        unsigned char b28 :1;
        unsigned char b29 :1;
        unsigned char b30 :1;
        unsigned char b31 :1;
    } bits;
} uint32b_t;

typedef enum {
    STATE_00,
    STATE_01,
    STATE_02,
    STATE_03,
    STATE_04,
    STATE_05,
    STATE_06,
    STATE_07,
    STATE_08,
    STATE_09,
    STATE_10,
    STATE_11,
    STATE_12,
    STATE_13,
    STATE_14,
    STATE_15,
    STATE_16,
    STATE_17,
    STATE_18,
    STATE_19,
    STATE_20,
    STATE_21,
    STATE_22,
    STATE_23,
    STATE_24,
    STATE_25,
    STATE_26,
    STATE_27,
    STATE_28,
    STATE_29,
    STATE_30,
    STATE_31,
    STATE_32,
    STATE_33,
    STATE_34,
    STATE_35,
    STATE_36,
    STATE_37,
    STATE_38,
    STATE_39,
    STATE_40,
    STATE_41,
    STATE_42,
    STATE_43,
    STATE_44,
    STATE_45,
    STATE_46,
    STATE_47,
    STATE_48,
    STATE_49,
    STATE_50
} T_State;

#endif    /* MYTYPES_H */

/* --------------------------------- End Of File ---------------------------- */
