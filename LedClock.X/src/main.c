/*******************************************************************************
 * @file main.c
 * @brief M�dulo principal
 * @details Respons�vel pela inicializa��o e execu��o de m�dulos.
 * @author MM
 * @version 00.00
 * @date 22/01/13
 * @copyright Dynasys
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <p18cxxx.h>
#include "mytypes.h"
#include "sys/configBits.h"
#include "sys/timerTicks.h"
#include "clock.h"

/*******************************************************************************
 * Defines and typedefs
 ******************************************************************************/

/*******************************************************************************
 * Global variables
 ******************************************************************************/

static volatile T_State G_Main_ledStat;

/*******************************************************************************
 * Private functions prototypes
 ******************************************************************************/

static void Main_conf(void);
static void Main_ini(void);

/*******************************************************************************
 * Private functions
 ******************************************************************************/

/**
 * @fn     Main_conf
 * @brief  Configura��o inicial dos registos do microcontrolador
 * @param  None
 * @return None
 */
static void Main_conf(void)
{
    WDTCONbits.SWDTEN = 0;
    OSCCON = 0x60;  //8MHzx3=24MHz
    OSCCON2 = 0x14;
    OSCTUNE = 0x00;
    RCONbits.IPEN = 0; //desabilita interrup��es com n�vel de prioridade
    INTCONbits.GIEL = 0; /* Disable low interrupts.*/
    INTCONbits.GIEH = 0; /* Disable high interrupts.*/
    INTCON2bits.RBPU = 1; /*PORTB pull-ups desabilitados*/
    ADCON0bits.ADON = 0; /*Disable A/D converter module*/
    T0CON = 0x00; /*desabilita totalmente Timer0*/
    T1CON = 0x00; /*desabilita totalmente Timer1*/
    ANSELA = 0x00; /*pinos configurados como I/Os digitais*/
    ANSELB = 0x00; /*pinos configurados como I/Os digitais*/
    ANSELC = 0x00; /*pinos configurados como I/Os digitais*/
    VREGCONbits.VREGPM=3;
    PMD0=0x75;
    PMD1=0x3F;

}

/**
 * @fn     main_ini
 * @brief  Inicializacao do modulo
 * @param  None
 * @return None
 */
static void Main_ini(void)
{

    INTCONbits.GIEH = 1; //habilita interrup��es com alta prioridade
    INTCONbits.GIEL = 1; //habilita interrup��es com baixa prioridade
    OSCCONbits.IDLEN = 1;

#ifdef Debug
    WDTCONbits.SWDTEN = 0;
#else
    WDTCONbits.SWDTEN = 1;
#endif
    TimerTicks_ini(); //inicia timer0 de gera��o de delays
    Clock_ini();

    G_Main_ledStat = STATE_00;
}


/*******************************************************************************
 * Public functions
 ******************************************************************************/

/**
 * @fn     main_ini
 * @brief  Inicializacao do modulo
 * @param  None
 * @return None
 */
void main(void)
{
    
    Main_conf();
    Main_ini();

    while (1)
    {
        Clock_run();
//Clock_drawTimeExecute();
#ifndef Debug
        ClrWdt(); /*configurado para 2048ms*/
#endif
    }
}


/* ------------------------------- End Of File ------------------------------ */
