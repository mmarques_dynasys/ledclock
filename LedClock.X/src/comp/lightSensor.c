/*******************************************************************************
 * @file LightSensor.c
 * @brief Functions to control the MCP7940N RTCC.
 * @author MM
 * @version 00.00
 * @date 01/10/14
 * @copyright MM
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <p18cxxx.h>
#include <i2c.h>
#include "mytypes.h"
#include "comp/lightSensor.h"

/*******************************************************************************
 * Defines
 ******************************************************************************/

#define LIGHTSENSOR_CTRL    0x52

#define LUX_SCALE 14 // scale by 2^14
#define RATIO_SCALE 9 // scale ratio by 2^9

//---------------------------------------------------
// Integration time scaling factors
//---------------------------------------------------

#define CH_SCALE 10 // scale channel values by 2^10
#define CHSCALE_TINT0 0x7517 // 322/11 * 2^CH_SCALE
#define CHSCALE_TINT1 0x0fe7 // 322/81 * 2^CH_SCALE

//---------------------------------------------------
// T, FN, and CL Package coefficients
//---------------------------------------------------
// For Ch1/Ch0=0.00 to 0.50
// Lux/Ch0=0.0304-0.062*((Ch1/Ch0)^1.4)
// piecewise approximation
// For Ch1/Ch0=0.00 to 0.125:
// Lux/Ch0=0.0304-0.0272*(Ch1/Ch0)
//
// For Ch1/Ch0=0.125 to 0.250:
// Lux/Ch0=0.0325-0.0440*(Ch1/Ch0)
//
// For Ch1/Ch0=0.250 to 0.375:
// Lux/Ch0=0.0351-0.0544*(Ch1/Ch0)
//
// For Ch1/Ch0=0.375 to 0.50:
// Lux/Ch0=0.0381-0.0624*(Ch1/Ch0)
//
// For Ch1/Ch0=0.50 to 0.61:
// Lux/Ch0=0.0224-0.031*(Ch1/Ch0)
//
// For Ch1/Ch0=0.61 to 0.80:
// Lux/Ch0=0.0128-0.0153*(Ch1/Ch0)
//
// For Ch1/Ch0=0.80 to 1.30:
// Lux/Ch0=0.00146-0.00112*(Ch1/Ch0)
//
// For Ch1/Ch0>1.3:
// Lux/Ch0=0
//---------------------------------------------------

#define K1T 0x0040 // 0.125 * 2^RATIO_SCALE
#define B1T 0x01f2 // 0.0304 * 2^LUX_SCALE
#define M1T 0x01be // 0.0272 * 2^LUX_SCALE
#define K2T 0x0080 // 0.250 * 2^RATIO_SCALE
#define B2T 0x0214 // 0.0325 * 2^LUX_SCALE
#define M2T 0x02d1 // 0.0440 * 2^LUX_SCALE
#define K3T 0x00c0 // 0.375 * 2^RATIO_SCALE
#define B3T 0x023f // 0.0351 * 2^LUX_SCALE
#define M3T 0x037b // 0.0544 * 2^LUX_SCALE
#define K4T 0x0100 // 0.50 * 2^RATIO_SCALE
#define B4T 0x0270 // 0.0381 * 2^LUX_SCALE
#define M4T 0x03fe // 0.0624 * 2^LUX_SCALE
#define K5T 0x0138 // 0.61 * 2^RATIO_SCALE
#define B5T 0x016f // 0.0224 * 2^LUX_SCALE
#define M5T 0x01fc // 0.0310 * 2^LUX_SCALE
#define K6T 0x019a // 0.80 * 2^RATIO_SCALE
#define B6T 0x00d2 // 0.0128 * 2^LUX_SCALE
#define M6T 0x00fb // 0.0153 * 2^LUX_SCALE
#define K7T 0x029a // 1.3 * 2^RATIO_SCALE
#define B7T 0x0018 // 0.00146 * 2^LUX_SCALE
#define M7T 0x0012 // 0.00112 * 2^LUX_SCALE
#define K8T 0x029a // 1.3 * 2^RATIO_SCALE
#define B8T 0x0000 // 0.000 * 2^LUX_SCALE
#define M8T 0x0000 // 0.000 * 2^LUX_SCALE

//---------------------------------------------------
// CS package coefficients
//---------------------------------------------------
// For 0 <= Ch1/Ch0 <= 0.52
// Lux/Ch0 = 0.0315-0.0593*((Ch1/Ch0)^1.4)
// piecewise approximation
// For 0 <= Ch1/Ch0 <= 0.13
// Lux/Ch0 = 0.0315-0.0262*(Ch1/Ch0)
// For 0.13 <= Ch1/Ch0 <= 0.26
// Lux/Ch0 = 0.0337-0.0430*(Ch1/Ch0)
// For 0.26 <= Ch1/Ch0 <= 0.39
// Lux/Ch0 = 0.0363-0.0529*(Ch1/Ch0)
// For 0.39 <= Ch1/Ch0 <= 0.52
// Lux/Ch0 = 0.0392-0.0605*(Ch1/Ch0)
// For 0.52 < Ch1/Ch0 <= 0.65
// Lux/Ch0 = 0.0229-0.0291*(Ch1/Ch0)
// For 0.65 < Ch1/Ch0 <= 0.80
// Lux/Ch0 = 0.00157-0.00180*(Ch1/Ch0)
// For 0.80 < Ch1/Ch0 <= 1.30
// Lux/Ch0 = 0.00338-0.00260*(Ch1/Ch0)
// For Ch1/Ch0 > 1.30
// Lux = 0
//---------------------------------------------------

#define K1C 0x0043 // 0.130 * 2^RATIO_SCALE
#define B1C 0x0204 // 0.0315 * 2^LUX_SCALE
#define M1C 0x01ad // 0.0262 * 2^LUX_SCALE
#define K2C 0x0085 // 0.260 * 2^RATIO_SCALE
#define B2C 0x0228 // 0.0337 * 2^LUX_SCALE
#define M2C 0x02c1 // 0.0430 * 2^LUX_SCALE
#define K3C 0x00c8 // 0.390 * 2^RATIO_SCALE
#define B3C 0x0253 // 0.0363 * 2^LUX_SCALE
#define M3C 0x0363 // 0.0529 * 2^LUX_SCALE
#define K4C 0x010a // 0.520 * 2^RATIO_SCALE
#define B4C 0x0282 // 0.0392 * 2^LUX_SCALE
#define M4C 0x03df // 0.0605 * 2^LUX_SCALE
#define K5C 0x014d // 0.65 * 2^RATIO_SCALE
#define B5C 0x0177 // 0.0229 * 2^LUX_SCALE
#define M5C 0x01dd // 0.0291 * 2^LUX_SCALE
#define K6C 0x019a // 0.80 * 2^RATIO_SCALE
#define B6C 0x0101 // 0.0157 * 2^LUX_SCALE
#define M6C 0x0127 // 0.0180 * 2^LUX_SCALE
#define K7C 0x029a // 1.3 * 2^RATIO_SCALE
#define B7C 0x0037 // 0.00338 * 2^LUX_SCALE
#define M7C 0x002b // 0.00260 * 2^LUX_SCALE
#define K8C 0x029a // 1.3 * 2^RATIO_SCALE
#define B8C 0x0000 // 0.000 * 2^LUX_SCALE
#define M8C 0x0000 // 0.000 * 2^LUX_SCALE

/*******************************************************************************
 * Typedefs
 ******************************************************************************/

typedef enum {
    CONTROL,
    TIMING,
    THRESHLOWLOW,
    THRESHLOWHIGH,
    THRESHHIGHLOW,
    THRESHHIGHHIGH,
    INTERRUPT,
    RESERVED1,
    CRC,
    RESERVED2,
    ID,
    RESERVED3,
    DATA0LOW,
    DATA0HIGH,
    DATA1LOW,
    DATA1HIGH
} T_LightSensor_reg;

/*******************************************************************************
 * Private functions prototypes
 ******************************************************************************/

static void LightSensor_wrReg(uint8_t reg, uint8_t data);
static uint8_t LightSensor_rdReg(uint8_t reg);

/*******************************************************************************
 * Private functions
 ******************************************************************************/

/**
 * @fn        LightSensor_wrReg
 * @brief     LightSensor register write.
 * @param[in] reg Register address.
 * @param[in] data Value to write in the register.
 * @return    None.
 */
static void LightSensor_wrReg(uint8_t reg, uint8_t data)
{
    IdleI2C1();
    StartI2C1();
    WriteI2C1(LIGHTSENSOR_CTRL);   //WRITE
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RTC
    WriteI2C1(0x80|reg);
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RTC
    WriteI2C1(data);
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RTC
    StopI2C1();
}

/**
 * @fn        LightSensor_rdReg
 * @brief     LightSensor register read.
 * @param[in] reg Register address.
 * @return    Value readed for register.
 */
static uint8_t LightSensor_rdReg(uint8_t reg)
{
    uint8_t data;
    IdleI2C1();
    StartI2C1();
    WriteI2C1(LIGHTSENSOR_CTRL);   //WRITE
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RT
    WriteI2C1(0x80|reg);
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RT
    RestartI2C1();
    WriteI2C1(LIGHTSENSOR_CTRL|0x01);  //READ
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RT
    data=ReadI2C1();
    NotAckI2C1();
    StopI2C1();
    return (data);
}

/*******************************************************************************
 * Public functions
 ******************************************************************************/

// lux equation approximation without floating point calculations
//////////////////////////////////////////////////////////////////////////////
// Routine: unsigned int CalculateLux(unsigned int ch0, unsigned int ch0, int iType)
//
// Description: Calculate the approximate illuminance (lux) given the raw
// channel values of the TSL2560. The equation if implemented
// as a piece-wise linear approximation.
//
// Arguments: unsigned int iGain - gain, where 0:1X, 1:16X
// unsigned int tInt - integration time, where 0:13.7mS, 1:100mS, 2:402mS,
// 3:Manual
// unsigned int ch0 - raw channel value from channel 0 of TSL2560
// unsigned int ch1 - raw channel value from channel 1 of TSL2560
// unsigned int iType - package type (T or CS)
//
// Return: unsigned int - the approximate illuminance (lux)
//
//////////////////////////////////////////////////////////////////////////////
uint16_t CalculateLux(uint8_t iGain, uint8_t tInt, uint16_t ch0, uint16_t ch1, uint8_t iType)
{
	//------------------------------------------------------------------------
	// first, scale the channel values depending on the gain and integration time
	// 16X, 402mS is nominal.
	// scale if integration time is NOT 402 msec
	uint32_t chScale, channel0, channel1, ratio, ratio1, lux;
    int32_t temp;
    uint16_t b, m;

    ratio1=0;
	switch (tInt)
	{
		case 0: // 13.7 msec
			chScale = CHSCALE_TINT0;
		break;
		case 1: // 101 msec
			chScale = CHSCALE_TINT1;
		break;
		default: // assume no scaling
			chScale = (1 << CH_SCALE);
		break;
	}
	// scale if gain is NOT 16X
	if (!iGain)
		chScale = chScale << 4; // scale 1X to 16X
	// scale the channel values
	channel0 = (ch0 * chScale) >> CH_SCALE;
	channel1 = (ch1 * chScale) >> CH_SCALE;

	//------------------------------------------------------------------------
	// find the ratio of the channel values (Channel1/Channel0)
	// protect against divide by zero
	if (channel0 != 0)
		ratio1 = (channel1 << (RATIO_SCALE+1)) / channel0;
	// round the ratio value
	ratio = (ratio1 + 1) >> 1;
	// is ratio <= eachBreak ?
	switch (iType)
	{
		case 0: // T, FN and CL package
            if (ratio <= K1T)
            {
                b=B1T;
                m=M1T;
            }
            else
                if (ratio <= K2T)
                {
                    b=B2T;
                    m=M2T;
                }
                else
                    if (ratio <= K3T)
                    {
                        b=B3T;
                        m=M3T;
                    }
                    else
                        if (ratio <= K4T)
                        {
                            b=B4T;
                            m=M4T;
                        }
                        else
                            if (ratio <= K5T)
                            {
                                b=B5T;
                                m=M5T;
                            }
                            else
                                if (ratio <= K6T)
                                {
                                    b=B6T;
                                    m=M6T;
                                }
                                else
                                    if (ratio <= K7T)
                                    {
                                        b=B7T;
                                        m=M7T;
                                    }
                                    else
                                        if (ratio > K8T)
                                        {
                                            b=B8T;
                                            m=M8T;
                                        }
		break;
		case 1:// CS package
			if (ratio <= K1C)
			{
				b=B1C;
				m=M1C;
			}
			else
				if (ratio <= K2C)
				{
					b=B2C;
					m=M2C;
				}
				else
					if (ratio <= K3C)
					{
						b=B3C;
						m=M3C;
					}
					else
						if (ratio <= K4C)
						{
							b=B4C;
							m=M4C;
						}
						else
							if (ratio <= K5C)
							{
								b=B5C;
								m=M5C;
							}
							else
								if (ratio <= K6C)
								{
									b=B6C;
									m=M6C;
								}
								else
									if (ratio <= K7C)
									{
										b=B7C;
										m=M7C;
									}
									else
										if (ratio > K8C)
										{
											b=B8C;
											m=M8C;
										}
		break;
	}
	temp = ((channel0 * b) - (channel1 * m));
	// do not allow negative lux value
	if (temp < 0)
		temp = 0;
	// round lsb (2^(LUX_SCALE-1))
	temp += (1 << (LUX_SCALE-1));
	// strip off fractional portion
	lux = temp >> LUX_SCALE;
	return(lux);
}

uint16_t LightSensor_readValue(uint8_t chnl)
{
    uint16b_t aux;
    //LightSensor_wrReg(CONTROL,0x03);  //power up
    switch(chnl)
    {
        case 0:
            aux.int8.l=LightSensor_rdReg(DATA0LOW);
            aux.int8.h = LightSensor_rdReg(DATA0HIGH);
        break;
        case 1:
            aux.int8.l=LightSensor_rdReg(DATA1LOW);
            aux.int8.h = LightSensor_rdReg(DATA1HIGH);
        break;
    }
    //LightSensor_wrReg(CONTROL,0x00);  //power down
    return (aux.int16);
}

/**
 * @fn     LightSensor_ini
 * @brief  Module initialization
 * @param  None
 * @return None
 */
void LightSensor_ini(void)
{
    LightSensor_wrReg(CONTROL,0x03);  //power up
    LightSensor_wrReg(TIMING,0x12);  //integration time 402ms, gain x16
    //LightSensor_wrReg(CONTROL,0x00);  //power down
}

/* ------------------------------- End Of File ------------------------------ */
