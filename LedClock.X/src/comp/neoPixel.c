/*******************************************************************************
 * @file timerTicks.c
 * @brief Funcoes para manipulacao de uma lista de timers.
 * @details Os timer contam ate existir overflow do MSB da vari�vel de contagem,
 * assinalando-se assim o fim da contagem. Atraves da definicao de
 * TIMERTICKS_16BITS comuta-se entre um timer que conta ate 21474836,48s(32bits)
 *  ou 327,68s(16bits), para uma contagem base de 10ms
 * @author MM
 * @version 03.00
 * @date 31/01/13
 * @copyright Dynasys
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <p18cxxx.h>
//#include "xc.h"
#include "mytypes.h"
#include "comp/neoPixel.h"

/*******************************************************************************
 * Defines
 ******************************************************************************/

//RGB led serial comunication
#define NeoPixel_cnfDin()	     TRISAbits.TRISA2=0
#define NeoPixel_rstDin()	     LATAbits.LATA2=0
#define NeoPixel_setDin()	     LATAbits.LATA2=1

/*******************************************************************************
 * Public functions
 ******************************************************************************/

/**
 * @fn        NeoPixel_bit
 * @brief     Generates a bit for the RGB led.
 * @param[in] status Bit value to generate
 * @return    None
 */
void NeoPixel_bit(uint8_t status)
{
    if (status)   // Bit '1'
    {
        NeoPixel_setDin();
        Nop();
        Nop();
        Nop();
        Nop();
        NeoPixel_rstDin();
    }
    else   // Bit '0'
    {
        NeoPixel_setDin();
        Nop();
        NeoPixel_rstDin();
    }
}

/**
 * @fn     NeoPixel_ini
 * @brief  Module initialization
 * @param  None
 * @return None
 */
void NeoPixel_ini(void)
{
    NeoPixel_cnfDin();
    NeoPixel_rstDin();
}

/* ------------------------------- End Of File ------------------------------ */
