/*******************************************************************************
 * @file rtcc.c
 * @brief Functions to control the MCP7940N RTCC.
 * @author MM
 * @version 00.00
 * @date 01/10/14
 * @copyright MM
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <p18cxxx.h>
#include <i2c.h>
#include "mytypes.h"
#include "comp/rtcc.h"

/*******************************************************************************
 * Defines
 ******************************************************************************/

#define RTCC_CTRL    0xDE

#define Rtcc_confSDA()  TRISBbits.TRISB0=1
#define Rtcc_confSCL()  TRISBbits.TRISB1=1

#define Rtcc_byteDec2BCD(v)   ((v/10*16) + (v%10))
#define Rtcc_byteBCD2Dec(v)   ((v/16*10) + (v%16))

/*******************************************************************************
 * Typedefs
 ******************************************************************************/

typedef enum {
    RTCSEC,
    RTCMIN,
    RTCHOUR,
    RTCWKDAY,
    RTCDATE,
    RTCMTH,
    RTCYEAR,
    CONTROL,
    OSCTRIM,
    RESERVED1,
    ALM0SEC,
    ALM0MIN,
    ALM0HOUR,
    ALM0WKDAY,
    ALM0DATE,
    ALM0MTH,
    RESERVED2,
    ALM1SEC,
    ALM1MIN,
    ALM1HOUR,
    ALM1WKDAY,
    ALM1DATE,
    ALM1MTH,
    RESERVED3,
    PWRDNMIN,
    PWRDNHOUR,
    PWRDNDATE,
    PWRDNMTH,
    PWRUPMIN,
    PWRUPHOUR,
    PWRUPDATE,
    PWRUPMTH
} T_Rtcc_reg;

/*******************************************************************************
 * Private functions prototypes
 ******************************************************************************/

static void Rtcc_wrReg(uint8_t reg, uint8_t data);
static uint8_t Rtcc_rdReg(uint8_t reg);

/*******************************************************************************
 * Private functions
 ******************************************************************************/

/**
 * @fn        Rtcc_wrReg
 * @brief     Rtcc register write.
 * @param[in] reg Register address.
 * @param[in] data Value to write in the register.
 * @return    None.
 */
static void Rtcc_wrReg(uint8_t reg, uint8_t data)
{
    IdleI2C1();
    StartI2C1();
    WriteI2C1(RTCC_CTRL);   //WRITE
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RTC
    WriteI2C1(reg);
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RTC
    WriteI2C1(data);
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RTC
    StopI2C1();

}

/**
 * @fn        Rtcc_rdReg
 * @brief     Rtcc register read.
 * @param[in] reg Register address.
 * @return    Value readed for register.
 */
static uint8_t Rtcc_rdReg(uint8_t reg)
{
    uint8_t data;
    IdleI2C1();
    StartI2C1();
    WriteI2C1(RTCC_CTRL);   //WRITE
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RT
    WriteI2C1(reg);
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RT
    RestartI2C1();
    WriteI2C1(RTCC_CTRL|0x01);  //READ
    while (SSPCON2bits.ACKSTAT != 0); // wait for ack from RT
    data=ReadI2C1();
    NotAckI2C1();
    StopI2C1();
    return (data);
}

/*******************************************************************************
 * Public functions
 ******************************************************************************/

/**
 * @fn        Rtcc_dec2BCD
 * @brief     Decimal to BCD convertion.
 * @param[in] dec Time struct in decimal.
 * @return    Time struct in BCD.
 */
T_Rtcc_time Rtcc_dec2BCD(T_Rtcc_time dec)
{
    T_Rtcc_time time;
    time.sec = Rtcc_byteDec2BCD(dec.sec);
    time.min = Rtcc_byteDec2BCD(dec.min);
    time.hour = Rtcc_byteDec2BCD(dec.hour);
    time.dayW = dec.dayW;
    time.day = Rtcc_byteDec2BCD(dec.day);
    time.month = Rtcc_byteDec2BCD(dec.month);
    time.year = Rtcc_byteDec2BCD(dec.year);
    return (time);
}

/**
 * @fn        Rtcc_BCD2Dec
 * @brief     BCD to decimal convertion.
 * @param[in] bcd Time struct in BCD.
 * @return    Time struct in decimal.
 */
T_Rtcc_time Rtcc_BCD2Dec(T_Rtcc_time bcd)
{
    T_Rtcc_time time;
    time.sec = Rtcc_byteBCD2Dec(bcd.sec);
    time.min = Rtcc_byteBCD2Dec(bcd.min);
    time.hour = Rtcc_byteBCD2Dec(bcd.hour);
    time.dayW = bcd.dayW;
    time.day = Rtcc_byteBCD2Dec(bcd.day);
    time.month = Rtcc_byteBCD2Dec(bcd.month);
    time.year = Rtcc_byteBCD2Dec(bcd.year);
    return (time);
}

/**
 * @fn     Rtcc_readTime
 * @brief  Rtcc time and date registers read.
 * @param  None.
 * @return Time struct in BCD.
 */
void Rtcc_writeTime(T_Rtcc_time time)
{
    uint8_t aux;
    aux=Rtcc_rdReg(RTCSEC);
    Rtcc_wrReg(RTCSEC,time.sec|(aux&0x80));
    Rtcc_wrReg(RTCMIN,time.min);
    aux=Rtcc_rdReg(RTCHOUR);
    Rtcc_wrReg(RTCHOUR,time.hour|(aux&0x40));
    Rtcc_wrReg(RTCWKDAY,time.dayW);
    Rtcc_wrReg(RTCDATE,time.day);
    Rtcc_wrReg(RTCMTH,time.month);
    Rtcc_wrReg(RTCYEAR,time.year);
}

/**
 * @fn     Rtcc_readTime
 * @brief  Rtcc time and date registers read.
 * @param  None.
 * @return Time struct in BCD.
 */
T_Rtcc_time Rtcc_readTime(void)
{
    T_Rtcc_time time;
    time.sec=Rtcc_rdReg(RTCSEC)&0x7F;
    time.min=Rtcc_rdReg(RTCMIN)&0x7F;
    time.hour=Rtcc_rdReg(RTCHOUR)&0x1F;
    time.dayW=Rtcc_rdReg(RTCWKDAY)&0x07;
    time.day=Rtcc_rdReg(RTCDATE)&0x3F;
    time.month=Rtcc_rdReg(RTCMTH)&0x1F;
    time.year=Rtcc_rdReg(RTCYEAR);
    return (time);
}

/**
 * @fn     Rtcc_ini
 * @brief  Module initialization
 * @param  None
 * @return None
 */
void Rtcc_ini(void)
{
    uint8_t hour;
    Rtcc_confSDA();
    Rtcc_confSCL();
    SSP1ADD=0x3B;       //Fscl=100kHz
    OpenI2C1(MASTER,SLEW_OFF);
    Rtcc_wrReg(RTCSEC,0x80);  //enables oscillator
    Rtcc_wrReg(CONTROL,0x00);  //alarm off, no square output
    hour=Rtcc_rdReg(RTCHOUR);
    hour|=0x40; //12h Mode
    Rtcc_wrReg(RTCHOUR,hour);  //12h Mode
}

/* ------------------------------- End Of File ------------------------------ */
