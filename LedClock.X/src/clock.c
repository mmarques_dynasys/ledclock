/*******************************************************************************
 * @file clock.c
 * @brief Main control of the clock.
 * @author MM
 * @version 00.00
 * @date 01/10/14
 * @copyright MM
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <p18cxxx.h>
//#include "xc.h"
#include "mytypes.h"
#include "comp/neoPixel.h"
#include "sys/timerTicks.h"
#include "comp/rtcc.h"
#include "comp/lightSensor.h"
#include "clock.h"

/*******************************************************************************
 * Defines
 ******************************************************************************/

#define CLOCK_BUFSZ 12

#define CLOCK_BRIGHTSCALE   0.255       //max lux=1000

#define CLOCK_LEDMINBRIGHT   5
//#define CLOCK_LEDMAXBRIGHT   128

#define CLOCK_ON_MASK		0x07

#define Clock_cnfPwrEn()	 TRISAbits.TRISA4=0
#define Clock_rstPwrEn()	 LATAbits.LATA4=1
#define Clock_setPwrEn()	 LATAbits.LATA4=0

#define Clock_cnfLed0()	     TRISCbits.TRISC0=0
#define Clock_rstLed0()	     LATCbits.LATC0=0
#define Clock_setLed0()	     LATCbits.LATC0=1

#define Clock_cnfLed1()	     TRISCbits.TRISC1=0
#define Clock_rstLed1()	     LATCbits.LATC1=0
#define Clock_setLed1()	     LATCbits.LATC1=1

#define Clock_cnfLed2()	     TRISCbits.TRISC2=0
#define Clock_rstLed2()	     LATCbits.LATC2=0
#define Clock_setLed2()	     LATCbits.LATC2=1

#define Clock_cnfLed3()	     TRISCbits.TRISC6=0
#define Clock_rstLed3()	     LATCbits.LATC6=0
#define Clock_setLed3()	     LATCbits.LATC6=1

#define Clock_cnfLed4()	     TRISCbits.TRISC7=0
#define Clock_rstLed4()	     LATCbits.LATC7=0
#define Clock_setLed4()	     LATCbits.LATC7=1

#define Clock_cnfBtn1()   TRISBbits.TRISB5=1
#define Clock_isBtn1()    (!PORTBbits.RB5)

#define Clock_cnfBtn2()   TRISBbits.TRISB2=1
#define Clock_isBtn2()    (!PORTBbits.RB2)

#define Clock_fallEdgeIExt2()  INTCON2bits.INTEDG2=0  /*Int flanco descendente*/
#define Clock_enbIExt2()       INTCON3bits.INT2IE=1    /*activa Int2*/
#define Clock_disIExt2()       INTCON3bits.INT2IE=0    /*desactiva Int2*/
#define Clock_isEIExt2()       INTCON3bits.INT2IE      /*esta Int2 habilitada?*/
#define Clock_isIExt2()        INTCON3bits.INT2IF       /*Int2 activa?*/
#define Clock_clrIExt2()       INTCON3bits.INT2IF=0     /*limpa Int2*/
#define Clock_priorityIExt2()  INTCON3bits.INT2IP=1      /*High priority*/

#define Clock_timerTicksStop()   T2CONbits.TMR2ON=0
#define Clock_timerTicksStart()  T2CONbits.TMR2ON=1

/*******************************************************************************
 * Typedefs
 ******************************************************************************/

typedef struct {
    uint8_t green;
    uint8_t red;
    uint8_t blue;
} T_Clock_neoPixelLed;

typedef union {
    struct {
        unsigned btn1 :1;
        unsigned btn2 :1;
        unsigned b0 :6;
    } btn;
    uint8_t int8;
} T_Clock_btnStat;

/*******************************************************************************
 * Global variables
 ******************************************************************************/

static T_Clock_neoPixelLed G_Clock_ledBuf[CLOCK_BUFSZ];

static T_State G_Clock_scanStat;
static T_State G_Clock_drawStat;
static T_State G_Clock_debStat;
static T_State G_Clock_runStat;

static uint8_t G_Clock_ledBright;

static uint8b_t G_Clock_btn1Deb;
static uint8b_t G_Clock_btn2Deb;
static T_Clock_btnStat G_Clock_btnStat;

/*******************************************************************************
 * Private functions prototypes
 ******************************************************************************/

static void Clock_btnDebounce(void);
static void Clock_ledBrightCalc(void);
static void Clock_bufferClear(void);
static void Clock_bufferIni(void);
static void Clock_minuteLed(uint8_t mled);
static void Clock_rotate(void);
static void Clock_draw(void);
static T_Cycle Clock_wrTime(uint8_t hour, uint8_t min);
static T_Cycle Clock_wakeUpSequence(void);

/*******************************************************************************
 * Private functions
 ******************************************************************************/

/**
 * @fn     Clock_btnDebounce
 * @brief  Debounce dos botoes.
 * @param  None.
 * @return None.
 */
static void Clock_btnDebounce(void)
{
	switch(G_Clock_debStat)
	{
		case STATE_00:
            TimerTicks_setBtnDeb(TimerTicks_msecToTics(10));
            G_Clock_debStat = STATE_01;
		break;
        case STATE_01:
            G_Clock_btn1Deb.int8 <<= 1;
            G_Clock_btn2Deb.int8 <<= 1;
            G_Clock_debStat = STATE_02;
		break;
        case STATE_02:
            G_Clock_btn1Deb.bits.b0 = Clock_isBtn1();
            if(G_Clock_btn1Deb.int8 == CLOCK_ON_MASK )
                G_Clock_btnStat.btn.btn1=1;
            G_Clock_debStat = STATE_03;
        break;
        case STATE_03:
            G_Clock_btn2Deb.bits.b0 = Clock_isBtn2();
            if(G_Clock_btn2Deb.int8 == CLOCK_ON_MASK )
                G_Clock_btnStat.btn.btn2=1;
            G_Clock_debStat = STATE_04;
        break;
		case STATE_04:
			if( TimerTicks_isBtnDeb() )
				G_Clock_debStat = STATE_00;
		break;
	    default:
	    	G_Clock_debStat = STATE_00;
	    break;
	}
}

static void Clock_ledBrightCalc(void)
{
    uint16_t adc0,adc1,lux;
    uint8_t scale;
    adc0=LightSensor_readValue(0);
    adc1=LightSensor_readValue(1);
    lux=CalculateLux(1,2, adc0, adc1,0);
    G_Clock_ledBright=(uint8_t)(lux*CLOCK_BRIGHTSCALE);
    if(G_Clock_ledBright<CLOCK_LEDMINBRIGHT)
        G_Clock_ledBright=CLOCK_LEDMINBRIGHT;
}

/**
 * @fn     Clock_bufferClear
 * @brief  Clears the RGB leds buffer.
 * @param  None
 * @return None
 */
static void Clock_bufferClear(void)
{
    uint8_t ind;
    for (ind = 0; ind < CLOCK_BUFSZ; ind++)
    {
        G_Clock_ledBuf[ind].green = 0;
        G_Clock_ledBuf[ind].red = 0;
        G_Clock_ledBuf[ind].blue = 0;
    }
}

/**
 * @fn     Clock_bufferClear
 * @brief  Puts the RGB leds buffer to initial state.
 * @param  None
 * @return None
 */
static void Clock_bufferIni(void)
{
    uint8_t ind;
    for (ind = 0; ind < CLOCK_BUFSZ; ind++)
    {
        if (ind < 4)
        {
            G_Clock_ledBuf[ind].green = G_Clock_ledBright;
            G_Clock_ledBuf[ind].red = 0;
            G_Clock_ledBuf[ind].blue = 0;
        }
        else if ((ind >= 4) & (ind < 8))
        {
            G_Clock_ledBuf[ind].green = 0;
            G_Clock_ledBuf[ind].red = G_Clock_ledBright;
            G_Clock_ledBuf[ind].blue = 0;
        }
        else if (ind >= 8)
        {
            G_Clock_ledBuf[ind].green = 0;
            G_Clock_ledBuf[ind].red = 0;
            G_Clock_ledBuf[ind].blue = G_Clock_ledBright;
        }
    }
}

/**
 * @fn        Clock_minuteLed
 * @brief     Activates the SMD leds according to the minutes.
 * @param[in] mled Minutes to represent.
 * @return    None
 */
static void Clock_minuteLed(uint8_t mled)
{
    switch(mled)
    {
        case 0:
            Clock_rstLed0();
            Clock_rstLed1();
            Clock_rstLed2();
            Clock_rstLed3();
            Clock_rstLed4();
        break;
        case 1:
            Clock_rstLed0();
            Clock_rstLed1();
            Clock_setLed2();
            Clock_rstLed3();
            Clock_rstLed4();
        break;
        case 2:
            Clock_setLed0();
            Clock_rstLed1();
            Clock_rstLed2();
            Clock_rstLed3();
            Clock_setLed4();
        break;
        case 3:
            Clock_setLed0();
            Clock_rstLed1();
            Clock_setLed2();
            Clock_rstLed3();
            Clock_setLed4();
        break;
        case 4:
            Clock_setLed0();
            Clock_setLed1();
            Clock_rstLed2();
            Clock_setLed3();
            Clock_setLed4();
        break;
    }
}

/**
 * @fn     Clock_rotate
 * @brief  Rotates the RGB leds buffer.
 * @param  None
 * @return None
 */
static void Clock_rotate(void)
{
    uint8_t ind;
    T_Clock_neoPixelLed auxPixel;
    auxPixel = G_Clock_ledBuf[0];
    for (ind = 0; ind < CLOCK_BUFSZ - 1; ind++)
        G_Clock_ledBuf[ind] = G_Clock_ledBuf[ind + 1];
    G_Clock_ledBuf[ind] = auxPixel;
}

/**
 * @fn     Clock_draw
 * @brief  Send the RGB leds buffer.
 * @param  None
 * @return None
 */
static void Clock_draw(void)
{
    uint8_t ind, mask;

    INTCONbits.GIEH = 0; //high priority interrupt disabled
    INTCONbits.GIEL = 0; //low priority interrupt disabled

    for (ind = 0; ind < CLOCK_BUFSZ; ind++)
    {
        for (mask = 0x80; mask != 0x00; mask = mask >> 1)
            NeoPixel_bit(G_Clock_ledBuf[ind].green & mask);
        for (mask = 0x80; mask != 0x00; mask = mask >> 1)
            NeoPixel_bit(G_Clock_ledBuf[ind].red & mask);
        for (mask = 0x80; mask != 0x00; mask = mask >> 1)
            NeoPixel_bit(G_Clock_ledBuf[ind].blue & mask);
    }
    INTCONbits.GIEH = 1; //high priority interrupt enabled
    INTCONbits.GIEL = 1; //low priority interrupt enabled
}

/**
 * @fn        Clock_wrTime
 * @brief     Represents in all leds the current time.
 * @param[in] hour Time hours value.
 * @param[in] min Time minutes value.
 * @retval    ENDED Function terminated with success.
 * @retval    ONGOING Executing function.
 */
static T_Cycle Clock_wrTime(uint8_t hour, uint8_t min)
{
    static uint8_t mLed;
    uint8_t hInd,mInd;
    switch (G_Clock_scanStat)
    {
        case STATE_00:
            Clock_bufferClear();
            hInd=hour;
            mInd=min/5;
            mLed=min-(mInd*5);
            if(hInd==mInd)
                G_Clock_ledBuf[hInd].green = G_Clock_ledBright;
            else
            {
                G_Clock_ledBuf[mInd].red = G_Clock_ledBright;
                G_Clock_ledBuf[hInd].blue = G_Clock_ledBright;
            }
            G_Clock_scanStat = STATE_01;
        break;
        case STATE_01:
            Clock_draw();
            Clock_minuteLed(mLed);
            TimerTicks_setPixel(TimerTicks_secToTics(3));
            G_Clock_scanStat = STATE_02;
        break;
        case STATE_02:
            if(TimerTicks_isPixel())
            {
                Clock_bufferClear();
                Clock_draw();
                Clock_minuteLed(0);
                G_Clock_scanStat = STATE_00;
                return (ENDED);
            }
        break;
        default:
            G_Clock_scanStat = STATE_00;
        break;
    }
    return (ONGOING);
}

/**
 * @fn     Clock_wakeUpSequence
 * @brief  Produces the wake up sequence.
 * @param  None
 * @retval ENDED Sequence terminated with success.
 * @retval ONGOING Executing sequence.
 */
static T_Cycle Clock_wakeUpSequence(void)
{
    static uint8_t pos=0;
    switch (G_Clock_scanStat)
    {
        case STATE_00:
            Clock_bufferIni();
            pos=0;
            Clock_draw();
            TimerTicks_setPixel(TimerTicks_msecToTics(50));
            G_Clock_scanStat = STATE_01;
        break;
        case STATE_01:
            if(TimerTicks_isPixel())
            {
                Clock_rotate();
                Clock_draw();
                TimerTicks_setPixel(TimerTicks_msecToTics(50));
                pos++;
                if(pos==8)
                {
                    pos=0;
                    G_Clock_scanStat = STATE_02;
                }
            }
        break;
        case STATE_02:
            Clock_rotate();
            G_Clock_ledBuf[CLOCK_BUFSZ-1].green = 0;
            G_Clock_ledBuf[CLOCK_BUFSZ-1].red = 0;
            G_Clock_ledBuf[CLOCK_BUFSZ-1].blue = 0;
            Clock_draw();
            pos++;
            if(pos==12)
            {
                G_Clock_scanStat = STATE_00;
                return (ENDED);
            }
            else
            {
                TimerTicks_setPixel(TimerTicks_msecToTics(50));
                G_Clock_scanStat = STATE_03;
            }
        break;
        case STATE_03:
            if(TimerTicks_isPixel())
                G_Clock_scanStat = STATE_02;
        break;
        default:
            G_Clock_scanStat = STATE_00;
        break;
    }
    return (ONGOING);
}

/*******************************************************************************
 * Public functions
 ******************************************************************************/

void Clock_interrExt2(void)
{
    Clock_clrIExt2();
    Clock_disIExt2();
    G_Clock_btnStat.btn.btn2=1;
}

/**
 * @fn     Clock_drawTimeExecute
 * @brief  Rtcc read and time presentation.
 * @param  None
 * @retval ENDED Function terminated with success.
 * @retval ONGOING Executing function.
 */
T_Cycle Clock_drawTimeExecute(void)
{
    static T_Rtcc_time now;
    switch (G_Clock_drawStat)
    {
        case STATE_00:
            Clock_ledBrightCalc();
            now=Rtcc_readTime();
            now=Rtcc_BCD2Dec(now);
            G_Clock_drawStat=STATE_01;
        break;
        case STATE_01:
            if(Clock_wakeUpSequence()==ENDED)
                G_Clock_drawStat=STATE_02;
        break;
        case STATE_02:
            if(Clock_wrTime(now.hour,now.min)==ENDED)
            {
                G_Clock_drawStat=STATE_00;
                return (ENDED);
            }
        break;
        default:
            G_Clock_scanStat = STATE_00;
            G_Clock_drawStat=STATE_00;
        break;
    }
    return (ONGOING);
}


void Clock_run(void)
{
    static T_Rtcc_time now;
    static uint8_t mLed;
    uint8_t mInd;
    Clock_btnDebounce();
    switch (G_Clock_runStat)
    {
        case STATE_00:
            if (G_Clock_btnStat.btn.btn2 == 1)
            {
                Clock_timerTicksStart();
                Clock_setPwrEn();
                G_Clock_btnStat.btn.btn2=0;
                G_Clock_runStat = STATE_01;
            }
            else
                G_Clock_runStat = STATE_50;
        break;
        case STATE_01:
            if(Clock_drawTimeExecute()==ENDED)
                G_Clock_runStat = STATE_02;
        break;
        case STATE_02:
            if ((G_Clock_btnStat.btn.btn1 == 1)&&(G_Clock_btnStat.btn.btn2 == 1))
            {
                G_Clock_btnStat.int8=0x00;
                G_Clock_runStat = STATE_03;
            }
            else
                G_Clock_runStat = STATE_50;
        break;
        case STATE_03:
            now=Rtcc_readTime();
            now=Rtcc_BCD2Dec(now);
            G_Clock_runStat = STATE_04;
        break;
        case STATE_04:
            Clock_bufferClear();
            G_Clock_ledBuf[now.hour].blue = G_Clock_ledBright;
            Clock_draw();
            G_Clock_runStat = STATE_05;
        break;
        case STATE_05:
            if (G_Clock_btnStat.btn.btn1 == 1)
            {
                if(now.hour==11)
                    now.hour=0;
                else
                    now.hour++;
                G_Clock_btnStat.int8 = 0x00;
                G_Clock_runStat = STATE_04;
            }
            if(G_Clock_btnStat.btn.btn2 == 1)
            {
                Clock_bufferClear();
                Clock_draw();
                G_Clock_btnStat.int8 = 0x00;
                G_Clock_runStat = STATE_06;
            }
        break;
        case STATE_06:
            if(now.min==0)
            {
                mInd=now.min;
                mLed=now.min;
            }
            else
            {
                if(now.min<5)
                {
                    mInd=255;   //indica que nenhum dos minutos acende
                    mLed=now.min;
                }
                else
                {
                    mInd=now.min/5;
                    mLed=now.min-(mInd*5);
                }
            }
            Clock_bufferClear();
            if(mInd!=255)
                G_Clock_ledBuf[mInd].red = G_Clock_ledBright;
            Clock_draw();
            Clock_minuteLed(mLed);
            G_Clock_runStat = STATE_07;
        break;
        case STATE_07:
            if (G_Clock_btnStat.btn.btn1 == 1)
            {
                if(now.min==59)
                    now.min=0;
                else
                    now.min++;
                G_Clock_btnStat.int8 = 0x00;
                G_Clock_runStat = STATE_06;
            }
            if(G_Clock_btnStat.btn.btn2 == 1)
            {
                Clock_bufferClear();
                Clock_draw();
                Clock_minuteLed(0);
                G_Clock_btnStat.int8 = 0x00;
                G_Clock_runStat = STATE_08;
            }
        break;
        case STATE_08:
            now=Rtcc_dec2BCD(now);
            Rtcc_writeTime(now);
            G_Clock_runStat = STATE_50;
        break;
        case STATE_50:
            Clock_clrIExt2();
            Clock_enbIExt2();
            Clock_timerTicksStop();
            Clock_rstPwrEn();
            Sleep();
            G_Clock_runStat = STATE_00;
        break;
    }
}

/**
 * @fn     Clock_ini
 * @brief  Module initialization
 * @param  None
 * @return None
 */
void Clock_ini(void)
{
    Rtcc_ini();
    LightSensor_ini();
    NeoPixel_ini();
    G_Clock_scanStat = STATE_00;
    G_Clock_drawStat = STATE_00;
    G_Clock_debStat = STATE_00;
    G_Clock_runStat = STATE_00;
    G_Clock_btn1Deb.int8=0x00;
    G_Clock_btn2Deb.int8=0x00;
    G_Clock_btnStat.int8=0x00;
    Clock_bufferIni();
    Clock_cnfLed0();
    Clock_cnfLed1();
    Clock_cnfLed2();
    Clock_cnfLed3();
    Clock_cnfLed4();
    Clock_cnfBtn1();
    Clock_cnfBtn2();
    Clock_cnfPwrEn();
    Clock_rstLed0();
    Clock_rstLed1();
    Clock_rstLed2();
    Clock_rstLed3();
    Clock_rstLed4();
    Clock_rstPwrEn();
    Clock_ledBrightCalc();
    Clock_disIExt2();
    Clock_fallEdgeIExt2();
    Clock_clrIExt2();
    Clock_enbIExt2();
}

/* ------------------------------- End Of File ------------------------------ */
