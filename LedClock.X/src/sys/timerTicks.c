/*******************************************************************************
 * @file timerTicks.c
 * @brief Funcoes para manipulacao de uma lista de timers.
 * @details Os timer contam ate existir overflow do MSB da vari�vel de contagem,
 * assinalando-se assim o fim da contagem. Atraves da definicao de 
 * TIMERTICKS_16BITS comuta-se entre um timer que conta ate 21474836,48s(32bits)
 *  ou 327,68s(16bits), para uma contagem base de 10ms
 * @author MM
 * @version 03.00
 * @date 31/01/13
 * @copyright Dynasys
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <p18cxxx.h>
#include "mytypes.h"
#include "sys/timerTicks.h"

/*******************************************************************************
 * Defines and typedefs
 ******************************************************************************/
#if 0
#define TIMERTICKS_16BITS
#endif

#ifdef TIMERTICKS_16BITS

#define TIMERTICKS_ROLLOVER	      32768  /* (2^15) */
#define TimerTicks_isRollOver(t)  G_TimerTicks_list[t].bit.b15
#define TimerTicks_timeClear(t)   G_TimerTicks_list[t].int16=0
#define TimerTicks_timeIncr(t)    G_TimerTicks_list[t].int16++
#define TimerTicks_calcDelta(t,d)       \
                 G_TimerTicks_list[t].int16=(uint16_t)TIMERTICKS_ROLLOVER-(uint16_t)d
typedef uint16b_t T_TimerTicks; // Timer

#else

#define TIMERTICKS_ROLLOVER	      2147483648  /* (2^31) */
#define TimerTicks_isRollOver(t)  G_TimerTicks_list[t].bits.b31
#define TimerTicks_timeClear(t)   G_TimerTicks_list[t].int32=0
#define TimerTicks_timeIncr(t)    G_TimerTicks_list[t].int32++
#define TimerTicks_calcDelta(t,d)       \
                 G_TimerTicks_list[t].int32=(uint32_t)TIMERTICKS_ROLLOVER-d
typedef uint32b_t T_TimerTicks; // Timer

#endif

#define TIMERTICKS_PR2C	   250	/* 40E-6x250(PR2)=10.00ms (FCLK=24MHz) */

#define TimerTicks_run()     T2CONbits.TMR2ON=1     /* Timer run.             */
#define TimerTicks_stop()    T2CONbits.TMR2ON=0     /* Timer stop.            */
#define TimerTicks_comp(v)   PR2=(v)                /* Comparator Register    */
#define TimerTicks_cnfOsc()  T2CON=0x77         /* Postscaler=15, Prescaler=16*/

#define TimerTicks_lowIPri() IPR1bits.TMR2IP=0      /* Low priority interrupt.*/
#define TimerTicks_enbI()    PIE1bits.TMR2IE=1      /* Enable interrupt.      */
#define TimerTicks_disI()    PIE1bits.TMR2IE=0      /* Disable interrupt.     */
#define TimerTicks_isIE()    PIE1bits.TMR2IE        /* Test enable interrupt. */
#define TimerTicks_isI()     PIR1bits.TMR2IF        /* Interrupt pending.     */
#define TimerTicks_clrI()    PIR1bits.TMR2IF=0      /* Clear interrupt flag.  */

/*******************************************************************************
 * Global variables
 ******************************************************************************/

static volatile T_TimerTicks G_TimerTicks_list[TIMERTICKS_COUNT];

/*******************************************************************************
 * Private functions prototypes
 ******************************************************************************/

static void TimerTicks_iniList(void);

/*******************************************************************************
 * Private functions
 ******************************************************************************/

/**
 * @fn     TimerTicks_iniList
 * @brief  Inicializacao da lista de timer.
 * @param  None
 * @return None
 */
static void TimerTicks_iniList(void)
{
    uint8_t timer_ind = 0;
    while (timer_ind < (uint8_t) TIMERTICKS_COUNT)
        TimerTicks_timeClear(timer_ind++);
}

/*******************************************************************************
 * Public functions
 ******************************************************************************/

/**
 * @fn     TimerTicks_interr
 * @brief  Interrupcao do timer, todos os timers da lista sao incrementados.
 * @param  None
 * @return None
 */
void TimerTicks_interr(void)
{
    TimerTicks_timeIncr(TIMERTICKS_MAINLED);
    TimerTicks_timeIncr(TIMERTICKS_PIXEL);
    TimerTicks_timeIncr(TIMERTICKS_BTNDEB);
    TimerTicks_clrI();
}

/**
 * @fn        TimerTicks_isTimer
 * @brief     Verifica se o timer atingiu a contagem (set do MSB).
 * @param[in] timer Indice na lista do timer a verificar.
 * @retval    TRUE Timer atingiu contagem
 * @retval    FALSE Timer n�o atingiu contagem
 */
Boolean TimerTicks_isTimer(uint8_t timer)
{
    return ((TimerTicks_isRollOver(timer)) ? 1 : 0);
}

/**
 * @fn        TimerTicks_setTimer
 * @brief     Inicializa timer indicado, com a temporizacao desejada.
 * @param[in] timer Indice na lista do timer a inicializar.
 * @param[in] delta Equivalente em ticks para a temporizacao desejada.
 * @return None
 */
void TimerTicks_setTimer(uint8_t timer, uint32_t delta)
{
    TimerTicks_disI();
    TimerTicks_calcDelta(timer, delta);
    TimerTicks_enbI();
}

/**
 * @fn     TimerTicks_ini
 * @brief  Inicializacao do modulo
 * @param  None
 * @return None
 */
void TimerTicks_ini(void)
{
    TimerTicks_disI();
    TimerTicks_lowIPri();
    TimerTicks_stop();
    TimerTicks_clrI();
    TimerTicks_comp(TIMERTICKS_PR2C);
    TimerTicks_cnfOsc();
    TimerTicks_iniList();
    TimerTicks_enbI();
    TimerTicks_run();
}

/* ------------------------------- End Of File ------------------------------ */
