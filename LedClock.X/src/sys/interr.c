/*******************************************************************************
 * @file interr.c
 * @brief Modulo para identificacao de interrupcoes.
 * @details Identifica a interrupcao que "disparou" e trata-a.
 * @author MM
 * @version 00.00
 * @date 22/01/13
 * @copyright Dynasys
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <p18cxxx.h>
#include "sys/timerTicks.h"

/*******************************************************************************
 * Defines and typedefs
 ******************************************************************************/

#define Interr_isTimerSleepIE() (PIE1bits.TMR1IE)   /* Test enable interrupt. */
#define Interr_isTimerSleepI()  (PIR1bits.TMR1IF)   /* Interrupt pending.     */

#define Interr_isTimerTicksIE() (PIE1bits.TMR2IE)   /* Test enable interrupt. */
#define Interr_isTimerTicksI()  (PIR1bits.TMR2IF)   /* Interrupt pending.     */

#define Interr_isPcom18ATxIE()  (PIE1bits.TX1IE)    /* Is enable interrupt tx.*/
#define Interr_isPcom18ATxI()   (PIR1bits.TX1IF)    /* Is tx byte empty.      */

#define Interr_isPcom18ARxIE()  (PIE1bits.RC1IE)    /* Is enable interrupt rx.*/
#define Interr_isPcom18ARxI()   (PIR1bits.RC1IF)    /* Is rx byte reception.  */

#define Interr_isPcom18BTxIE()  (PIE3bits.TX2IE)    /* Is enable interrupt tx.*/
#define Interr_isPcom18BTxI()   (PIR3bits.TX2IF)    /* Is tx byte empty.      */

#define Interr_isPcom18BRxIE()  (PIE3bits.RC2IE)    /* Is enable interrupt rx.*/
#define Interr_isPcom18BRxI()   (PIR3bits.RC2IF)    /* Is rx byte reception.  */

/*******************************************************************************
 * Private functions
 ******************************************************************************/

void Interr_high (void);
void Interr_low ( void );

/*******************************************************************************
 * Public functions
 ******************************************************************************/

/*---------------------------------= HIGH =--------------------------------*/

#pragma code Interr_VectHigh=0x08
void Interr_VectHigh ( void )
{
    _asm
    GOTO Interr_high
    _endasm
}
#pragma code

#pragma interrupt Interr_high 
void Interr_high (void)
{
    if(Interr_isTimerTicksIE()&&Interr_isTimerTicksI())
        TimerTicks_interr();
    if(Interr_isTimerSleepIE()&&Interr_isTimerSleepI())
        TimerSleep_interr();
    if(Interr_isPcom18ATxIE()&&Interr_isPcom18ATxI())
        Pcom18a_interrTx();
    if(Interr_isPcom18ARxIE()&&Interr_isPcom18ARxI())
        Pcom18a_interrRx();
#ifdef USE_COMB
    if(Interr_isPcom18BTxIE()&&Interr_isPcom18BTxI())
        Pcom18b_interrTx();
    if(Interr_isPcom18BRxIE()&&Interr_isPcom18BRxI())
        Pcom18b_interrRx();
#endif
}

/*---------------------------------= LOW =---------------------------------*/

#pragma code Interr_VectLow=0x18
void Interr_VectLow ( void )
{
    _asm
    GOTO Interr_low
    _endasm
}
#pragma code

#pragma interruptlow Interr_low
void Interr_low (void)
{
    ;
}

/* ------------------------------- End Of File ------------------------------ */
