/*******************************************************************************
 * @file timerTicks.c
 * @brief Funcoes para manipulacao de uma lista de timers.
 * @details Os timer contam ate existir overflow do MSB da vari�vel de contagem,
 * assinalando-se assim o fim da contagem. Atraves da definicao de
 * TIMERTICKS_16BITS comuta-se entre um timer que conta ate 21474836,48s(32bits)
 *  ou 327,68s(16bits), para uma contagem base de 10ms
 * @author MM
 * @version 03.00
 * @date 31/01/13
 * @copyright Dynasys
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/

#include <p18cxxx.h>
#include "sys/timerTicks.h"
#include "clock.h"

/*******************************************************************************
 * Defines
 ******************************************************************************/

#define Interrupt_isTmr2IE()    PIE1bits.TMR2IE        /* Test enable interrupt. */
#define Interrupt_isTmr2I()     PIR1bits.TMR2IF        /* Interrupt pending.     */

#define Interrupt_isExt2IE()    INTCON3bits.INT2IE    /* Test enable interrupt. */
#define Interrupt_isExt2I()     INTCON3bits.INT2IF    /* Interrupt pending.     */

/*******************************************************************************
 * Public functions
 ******************************************************************************/

/**
 * @fn     TimerTicks_interr
 * @brief  Interrupcao do timer, todos os timers da lista sao incrementados.
 * @param  None
 * @return None
 */
void interrupt Interrupt(void)
{
    if (Interrupt_isTmr2IE() && Interrupt_isTmr2I())
        TimerTicks_interr();
    if (Interrupt_isExt2IE() && Interrupt_isExt2I())
        Clock_interrExt2();
}

/* ------------------------------- End Of File ------------------------------ */
